libbio-graphics-perl (2.40-6) unstable; urgency=medium

  * Team upload.
  * Add dependency on libbio-db-seqfeature-perl

 -- Michael R. Crusoe <michael.crusoe@gmail.com>  Mon, 25 Nov 2019 14:48:39 +0100

libbio-graphics-perl (2.40-5) unstable; urgency=medium

  * Team upload.
  * debian/control: Breaks+Replaces the old libbio-bio-perl. (Closes: #942789)
  * debian/patches/spelling: fix typos

 -- Michael R. Crusoe <michael.crusoe@gmail.com>  Sat, 23 Nov 2019 09:23:09 +0100

libbio-graphics-perl (2.40-4) unstable; urgency=medium

  * Team upload.
  * debhelper-compat 12
  * Standards-Version: 4.4.0
  * Secure URI in copyright format
  * Remove trailing whitespace in debian/changelog
  * Set fields Upstream-Name, Upstream-Contact in debian/copyright.
  * Remove obsolete fields Name, Contact from debian/upstream/metadata.
  * Add build-dep on libbio-db-seqfeature-perl which was split from bioperl
    (Closes: #942063)

 -- Michael R. Crusoe <michael.crusoe@gmail.com>  Tue, 12 Nov 2019 14:05:45 +0100

libbio-graphics-perl (2.40-3) unstable; urgency=medium

  * debhelper 11
  * Point Vcs fields to salsa.debian.org
  * Standards-Version: 4.2.1
  * s/search.cpan.org/metacpan.org/
  * Fix permissions

 -- Andreas Tille <tille@debian.org>  Fri, 21 Sep 2018 22:50:17 +0200

libbio-graphics-perl (2.40-2) unstable; urgency=medium

  * Team upload.
    (Assuming I'm still an honorary team member.)
  * Add more packages to Recommends which are needed by some modules.
    Thanks to autopkgtest-pkg-perl.
  * Fix autopkgtest.
    Don't use()-test Bio/Graphics/Glyph/vista_plot.pm which needs
    Bio::DB::BigWig which is not in Debian.
  * Move libmodule-build-perl to Build-Depends.
    It is needed by the clean target.
  * Add a patch to fix an "Unescaped left brace in regex is
    illegal here in regex" error. Thanks to autopkgtest-pkg-perl.

 -- gregor herrmann <gregoa@debian.org>  Thu, 03 Aug 2017 11:46:59 -0400

libbio-graphics-perl (2.40-1) unstable; urgency=medium

  * New upstream version
  * Build-Depends: libbio-coordinate-perl
    Closes: #848105
  * debhelper 10
  * d/watch: version=4
  * d/control: Testsuite: autopkgtest-pkg-perl

 -- Andreas Tille <tille@debian.org>  Tue, 20 Dec 2016 12:49:03 +0100

libbio-graphics-perl (2.39-5) unstable; urgency=medium

  * Build-Depends: rename
    Closes: #825432
  * cme fix dpkg-control

 -- Andreas Tille <tille@debian.org>  Fri, 27 May 2016 08:07:25 +0200

libbio-graphics-perl (2.39-4) unstable; urgency=medium

  * Add Build-Depends: libcgi-pm-perl | perl (<< 5.19)
    Closes: #789734
  * cme fix dpkg-control

 -- Andreas Tille <tille@debian.org>  Thu, 25 Jun 2015 10:04:58 +0200

libbio-graphics-perl (2.39-3) unstable; urgency=medium

  * Add libmodule-build-perl dep (Closes: #789063).

 -- Olivier Sallou <osallou@debian.org>  Fri, 19 Jun 2015 11:19:26 +0200

libbio-graphics-perl (2.39-2) unstable; urgency=medium

  * Build depends libgd-perl rather than the removed  libgd-gd2-perl package
    Closes: #768760

 -- Andreas Tille <tille@debian.org>  Sun, 09 Nov 2014 09:48:22 +0100

libbio-graphics-perl (2.39-1) unstable; urgency=medium

  * New upstream version (patch applied upstream)
  * cme fix dpkg-control
  * add libgd-svg-perl as additional Build-Depends-Indep to successfully run
    unit tests
  [ Olivier Sallou ]
  * d/rules: remove extension from binaries

 -- Andreas Tille <tille@debian.org>  Fri, 08 Aug 2014 18:17:13 +0200

libbio-graphics-perl (2.37-1) unstable; urgency=low

  * New  upstream release 2.37
  * Remove failing tests (Closes: #718147).

 -- Olivier Sallou <osallou@debian.org>  Mon, 29 Jul 2013 08:42:51 +0200

libbio-graphics-perl (2.33-1) unstable; urgency=low

  befc3bc Imported Upstream version 2.33
  68d9d7b Removed obsolete DM-Upload-Allowed field.
  3c16457 Normalised Debian control and copyright files with config-model-edit.
  2e26637 Removed obsolete Breaks statement.
  bdf87e1 Using Debhelper 9.
  d06499c Normalised VCS URLs following Lintian's standard.
  72c9b22 Conforms to Policy 3.9.4.

 -- Charles Plessy <plessy@debian.org>  Sun, 05 May 2013 22:29:14 +0900

libbio-graphics-perl (2.32-1) unstable; urgency=low

  63f795d New upstream release 2.32

 -- Olivier Sallou <osallou@debian.org>  Sat, 15 Dec 2012 11:20:43 +0100

libbio-graphics-perl (2.31-1) unstable; urgency=low

  e5dd936 New upstream release 2.31

 -- Olivier Sallou <osallou@debian.org>  Sat, 15 Dec 2012 10:00:47 +0100

libbio-graphics-perl (2.29-1) unstable; urgency=low

  5f027b7 Imported Upstream version 2.29.
  bdb8255 Summarised license for the whole package with a header field.
  9ce13ce Conforms to Debian Policy version 3.9.3.

 -- Charles Plessy <plessy@debian.org>  Sun, 09 Sep 2012 12:18:11 +0900

libbio-graphics-perl (2.26-1) unstable; urgency=low

  * New upstream release (Closes: #660481).

 -- Charles Plessy <plessy@debian.org>  Mon, 20 Feb 2012 13:05:39 +0900

libbio-graphics-perl (2.24-1) unstable; urgency=low

  * New usptream release.
  * Replaced deprecated ‘<’ by ‘<<’ in debian/control.
  * Renamed debian/README.Debian debian/README.test, as it only discusses tests
  * Incremented Standards-Version to reflect conformance with Policy 3.9.1
    (debian/control, no changes needed).

 -- Charles Plessy <plessy@debian.org>  Thu, 14 Jul 2011 19:50:40 +0900

libbio-graphics-perl (2.14-1) unstable; urgency=low

  * New upstream release.
  * Converted debian/copyright to DEP-5 machine-readable format.
  * Incremented Standards-Version to reflect conformance with Policy 3.9.1
    (debian/control, no changes needed).
  * Source package now managed with Git (debian/control).
  * Using Debhelper 8 (debian/control, debian/compat).

 -- Charles Plessy <plessy@debian.org>  Wed, 23 Mar 2011 15:45:48 +0900

libbio-graphics-perl (2.11-1) unstable; urgency=low

  * New upstream release.
  * Conformance with Policy 3.9.0:
    - Use Breaks instead of Conflicts (debian/control).
    - Incremented Standards-Version to reflect conformance (debian/control).
  * Mention the download directory instead of the full URL as Source
    in debian/copyright.

 -- Charles Plessy <plessy@debian.org>  Tue, 13 Jul 2010 10:39:44 +0900

libbio-graphics-perl (2.10-1) unstable; urgency=low

  * New upstream release.
  * Using Debhelper's ‘dh’ (debian/rules, debian/docs, debian/examples).
  * Recommends libfile-spec-perl (debian/control).

 -- Charles Plessy <plessy@debian.org>  Fri, 25 Jun 2010 20:02:53 +0900

libbio-graphics-perl (2.02-1) unstable; urgency=low

  * New upstream release (more support for BigWig and BigBed data,
    and other changes).
  * Build-depend on libstatistics-descriptive-perl, needed for the tests.
  * Recommend libbio-scf-perl, libgd-svg-perl,
    libstatistics-descriptive-perl, according to META.yaml.
  * Added a few words in debian/README.Debian on how this package is tested.

 -- Charles Plessy <plessy@debian.org>  Mon, 29 Mar 2010 22:23:43 +0900

libbio-graphics-perl (2.01-1) unstable; urgency=low

  * New upstream release.

 -- Charles Plessy <plessy@debian.org>  Sat, 06 Mar 2010 19:21:41 +0900

libbio-graphics-perl (2.00-1) unstable; urgency=low

  * New upstream release.
  * Incremented Standards-Version to reflect conformance
    with Policy 3.8.4 (debian/control, no other changes needed).

 -- Charles Plessy <plessy@debian.org>  Thu, 11 Feb 2010 22:45:02 +0900

libbio-graphics-perl (1.995-1) unstable; urgency=low

  * New upstream release.

 -- Charles Plessy <plessy@debian.org>  Fri, 08 Jan 2010 13:27:40 +0900

libbio-graphics-perl (1.994-1) unstable; urgency=low

  * New upstream releases.
  * debian/copyright:
    - Updated for new upstream file.
    - Added Artistic license version 2.0.

 -- Charles Plessy <plessy@debian.org>  Fri, 11 Dec 2009 13:42:21 +0900

libbio-graphics-perl (1.992-1) unstable; urgency=low

  * New upstream release.
  * Added a couple of informations in debian/upstream-metadata.yaml.

 -- Charles Plessy <plessy@debian.org>  Sun, 22 Nov 2009 22:33:46 +0900

libbio-graphics-perl (1.982-1) unstable; urgency=low

  * New upstream release fixing DAS stylesheet support.
  * Enhances: gbrowse (debian/control).

 -- Charles Plessy <plessy@debian.org>  Fri, 02 Oct 2009 21:07:17 +0900

libbio-graphics-perl (1.981-1) unstable; urgency=low

  * New upstream release fixing bugs in wiggle_xyplot and improving
    documentation.
  * debian/control: incremented Standards-Version to reflect conformance
    with Policy 3.8.3 (no changes needed).
  * Removed trailing spaces in debian/changelog.

 -- Charles Plessy <plessy@debian.org>  Mon, 24 Aug 2009 12:53:39 +0900

libbio-graphics-perl (1.97-1) unstable; urgency=low

  * New upstream release adding a “feature_limit” option.
  * debian/control: incremented Standards-Version to reflect conformance
    with Policy 3.8.2 (no changes needed).
  * Format experimentations on debian/copyright.

 -- Charles Plessy <plessy@debian.org>  Fri, 10 Jul 2009 19:12:27 +0900

libbio-graphics-perl (1.96-1) unstable; urgency=low

  * New upstream release improving DAS 1.5 support.
  * Documented in debian/copyright files distributed under the GPL version 1 or
    superior, or the Artistic license version 2. Ignored VCS leftovers
    ‘lib/Bio/Graphics/Glyph/.#dna.pm.1.1’ and
    ‘lib/Bio/Graphics/Glyph/.#hybrid_plot.pm.1.2’.

 -- Charles Plessy <plessy@debian.org>  Wed, 10 Jun 2009 13:04:42 +0900

libbio-graphics-perl (1.95-1) unstable; urgency=low

  * New upstream release, improving SVG support.

 -- Charles Plessy <plessy@debian.org>  Thu, 04 Jun 2009 19:05:06 +0900

libbio-graphics-perl (1.94-1) unstable; urgency=low

  * New upstream release, adding a “fast” option and fixing error in xyplot.

 -- Charles Plessy <plessy@debian.org>  Sun, 03 May 2009 18:24:13 +0900

libbio-graphics-perl (1.93-1) unstable; urgency=low

  * New upstream release (corrections and improved documentation).
  * debian/copyright made simpler according to current discussion
    on machine-readable format.

 -- Charles Plessy <plessy@debian.org>  Thu, 09 Apr 2009 10:40:38 +0900

libbio-graphics-perl (1.91-1) unstable; urgency=low

  * New upstream release with improvements on wiggle xyplots.
  * debian/control: checked conformance with Policy 3.8.1.

 -- Charles Plessy <plessy@debian.org>  Fri, 20 Mar 2009 18:49:47 +0900

libbio-graphics-perl (1.85-1) unstable; urgency=low

  * New upstream release:
    - Adds including capacities in the configuration files.
    - New object: Bio::Graphics::Glyph::hybrid_plot.
  * debian/copyright: incremented year and added new holder.
  * debian/rules: replaced deprecated `dh_clean -k' by `dh_prep'.

 -- Charles Plessy <plessy@debian.org>  Wed, 11 Mar 2009 10:23:12 +0900

libbio-graphics-perl (1.84-1) unstable; urgency=low

  * Initial Release. (Closes: #511382)

 -- Charles Plessy <plessy@debian.org>  Sat, 10 Jan 2009 17:49:21 +0900
